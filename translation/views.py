from django.shortcuts import render
from .forms import UploadForm
from django.http import HttpResponse
from source.render_vdo import render_vdo
from django.conf import settings
# import tensorflow as tf
# Create your views here.
def upload(request):
    template = 'upload.html'
    if request.method == 'POST':
    #     pass
        # print(req)
        form = UploadForm(request.POST, request.FILES)
        if form.is_valid():
            # form.save()
            template = 'display.html'

            form = form.save(commit=False)
            form.original_filename = request.FILES['video'].name
            form.name = request.FILES['video'].name
            form.save()
            # print(settings.MEDIA_ROOT+"/videos/"+request.FILES['video'].name)
            # graph = tf.get_default_graph()
            # with graph.as_default():
            vdo_name, width, height = render_vdo(settings.MEDIA_ROOT+"/videos/"+request.FILES['video'].name)
            
            # return HttpResponse('<a href="http://localhost:8000/media/videos/'+ vdo_name +'.webm" >video output</a>')
            # return HttpResponse('<source src="http://localhost:8000/media/videos/'+ vdo_name +'.webm" type="video/mp4"></source>')
            context = {'vdo':'http://worakit.pythonanywhere.com/media/videos/'+ vdo_name +'.webm','w':width,'h':height}
            
            return render(request, template, context)
    else:
        form = UploadForm()
    # form = UploadForm()
    context = {'form':form}

    return render(request, template, context)

def about(request):
    template = 'about.html'

    context = {}

    return render(request, template, context)
