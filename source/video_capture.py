import cv2
import numpy as np 
import time

# function
def face_detection():
    
    face_cascade = cv2.CascadeClassifier(r'C:\Users\Acer\Desktop\project_sample\source\haarcascade_frontalface_default.xml')
    # cap = cv2.VideoCapture(0)
    cap = cv2.VideoCapture(r"C:\Users\Acer\Desktop\project_sample\dataset\video\lastnames.mp4")
    h_min = 0
    s_min = 0
    v_min = 40
    h_max = 30
    s_max = 255
    v_max = 255


    while True:
        ret, img = cap.read()
        img = cv2.resize(img.copy(), (0,0), fx=0.5, fy=0.5)
#การลด นอยด์ ของภาพ ยิ่งเพิ่มค่ายิ่งเบลอ
        blur = cv2.GaussianBlur(img, (5, 5), 0) 
# แปลง BGR เป็น GRAY 
        gray = cv2.cvtColor(blur, cv2.COLOR_BGR2GRAY)  
  
        # retval2, thresh1 = cv2.threshold(gray, 170, 255, cv2.THRESH_BINARY)  
        face = face_cascade.detectMultiScale(gray, 1.3, 3)  

# สร้าง numpy array ที่มีค่า 0   
        mask_face = np.zeros(gray.shape, np.uint8)  
        mask_face.fill(255)

        hsv=cv2.cvtColor(img,cv2.COLOR_BGR2HSV)  
        upper = np.array([h_max,s_max,v_max],np.uint8)
        lower = np.array([h_min,s_min,v_min],np.uint8)       
        mask_face_hand = cv2.inRange(hsv,lower,upper)
        rows, cols = mask_face_hand.shape
        # cv2.rectangle(mask_face_hand, (cols - 100, 0), (cols, 100), (0), -1)
        
# MARKING THE DETECTED ROI
        for (x, y, w, h) in face:  
        #     cv2.rectangle(img, (x, y), (x + w, y + h), (0, 0, 200), 0)
            cv2.rectangle(mask_face, (x, 0), (x + w + 5, y + h + 20), (255), 0)
        
        mask = cv2.bitwise_and(mask_face, mask_face_hand)
        color_mask = cv2.bitwise_and(img, img, mask=mask)

        
        cv2.imshow("img", img)
        # cv2.imshow("mask_face", mask_face)
        # cv2.imshow("mask_face_hand", mask_face_hand)
        cv2.imshow('color_mask',color_mask)
        # cv2.imshow("mask", mask)
        

# video capture original
        name = str(time.time())
        name = name.replace(".","") + ".jpg"
        print(color_mask.shape)
        cv2.imwrite(r"C:\Users\Acer\Desktop\project_sample\dataset\mipenri\\" + name, color_mask)

        k = cv2.waitKey(100) & 0xff 
#รอ user กดปุ่ม q=Esc
        if k == ord('q'): 
            break

    cap.release()
    cv2.destroyAllWindows()

if __name__ == '__main__':
    face_detection()