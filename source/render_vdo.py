import cv2
import time
from .hand_translation  import translation
from django.conf import settings
import tensorflow as tf
from keras.models import load_model
from gtts import gTTS
from moviepy.editor import *

def render_vdo(vdo_path):
    print("Done upload")
    start_time = time.time()
    print("start process")
    print(vdo_path)
    cap = cv2.VideoCapture(vdo_path)
    first_frame = True
    out_name = str(time.time()).replace(".","")
    talk = {
        "sad":False,
        "thank":False,
        "fine":False,
        "sorry":False,
        "hello":False,
        "goodluck":False,
        "love":False,
        "name":False,
        "lestname":False,
        "understand":False,
    }

    word_count = {
        "sad":0,
        "thank":0,
        "fine":0,
        "sorry":0,
        "hello":0,
        "goodluck":0,
        "love":0,
        "name":0,
        "lastname":0,
        "understand":0,
    }
    word_active = None
    count_th = 3
    font = cv2.FONT_HERSHEY_SIMPLEX
    time_show_text = 5
    graph = tf.get_default_graph()
    result_text = "ผลการแปล  "
    count = 0
    with graph.as_default():
        model = load_model("/home/worakit/hand_translation/source/model.h5")
        model.compile(loss='categorical_crossentropy',
                    optimizer='adam',
                    metrics=['accuracy'])
        img_list = []
        while cap.isOpened():
            ret, frame = cap.read()
            if ret == False:
                break
            if count % 15 != 0:
                count += 1
                continue
            count += 1
            # print("frame")
            h,w,ch = frame.shape
            # count += 1
            # if count >= 15:
            #     break
            if w > 700 or h > 700:
                img = cv2.resize(frame.copy(), (0,0), fx=0.5, fy=0.5)
            else:
                img = frame.copy()
            # print(img.shape)
            _, word = translation(img, model)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            if first_frame:
                h,w,ch = img.shape
                width = w
                height = h
                frame_per_sec = 30
              
                first_frame = False
            
            result_text = result_text  + "   "
            if word == "ขอบคุณ" and talk["thank"] == False:

                if word_active is None or word_active == "thank":
                    word_count["thank"] += 1
                else:
                    word_count["thank"] = 1
                    word_active = "thank"

                if word_count["thank"] >= count_th:
                    # print("word_count",word_count["thank"])
                    word_count["thank"] = 0
                    # playsound(mp3_file)
                    talk["thank"] = True
                    cv2.putText(img, "Thank You",(100, 100), font, 0.75,(255,0,255),2,cv2.LINE_AA)
                    result_text = result_text + " ขอบคุณ"
                    for i in range(time_show_text):
                        img_list.append(img)


            elif word == "เสียใจ" and talk["sad"] == False:

                if word_active is None or word_active == "sad":
                    word_count["sad"] += 1
                else:
                    word_count["sad"] = 1
                    word_active = "sad"
                if word_count["sad"] >= count_th:
                    # print("word_count",word_count["sad"])
                    word_count["sad"] = 0
                    # playsound(mp3_file)
                    cv2.putText(img, "Sad",(100, 100), font, 0.75,(255,0,255),2,cv2.LINE_AA)
                    result_text = result_text + " เสียใจ"

                    for i in range(time_show_text):
                        # out.write(img)
                        img_list.append(img)
                    talk["sad"] = True

            elif word == "ขอโทษ" and talk["sorry"] == False:

                if word_active is None or word_active == "sorry":
                    word_count["sorry"] += 1
                else:
                    word_count["sorry"] = 1
                    word_active = "sorry"
                if word_count["sorry"] >= count_th:
                    # print("word_count",word_count["sorry"])
                    word_count["sorry"] = 0
                    # playsound(mp3_file)
                    cv2.putText(img, "Sorry",(100, 100), font, 0.75,(255,0,255),2,cv2.LINE_AA)
                    talk["sorry"] = True
                    result_text = result_text + " ขอโทษ"

                    for i in range(time_show_text):
                        # out.write(img)
                        img_list.append(img)
            
            elif word == "สวัสดี" and talk["hello"] == False:

                if word_active is None or word_active == "hello":
                    word_count["hello"] += 1
                else:
                    word_count["hello"] = 1
                    word_active = "hello"
                if word_count["hello"] >= count_th:
                    # print("word_count",word_count["hello"])
                    word_count["hello"] = 0
                    # playsound(mp3_file)
                    cv2.putText(img, "Hello",(100, 100), font, 0.75,(255,0,255),2,cv2.LINE_AA)
                    talk["hello"] = True
                    result_text = result_text + " สวัสดี"

                    for i in range(time_show_text):
                        # out.write(img)
                        img_list.append(img)
            elif word == "โชคดี" and talk["goodluck"] == False:

                if word_active is None or word_active == "goodluck":
                    word_count["goodluck"] += 1
                else:
                    word_count["goodluck"] = 1
                    word_active = "goodluck"
                if word_count["goodluck"] >= count_th:
                    # print("word_count",word_count["goodluck"])
                    word_count["goodluck"] = 0
                    # playsound(mp3_file)
                    cv2.putText(img, "Good Luck",(100, 100), font, 0.75,(255,0,255),2,cv2.LINE_AA)
                    result_text = result_text + " โชคดี"

                    talk["goodluck"] = True
                    for i in range(time_show_text):
                        # out.write(img)
                        img_list.append(img)
            
            elif word == "สบายดี" and talk["fine"] == False:

                if word_active is None or word_active == "fine":
                    word_count["fine"] += 1
                else:
                    word_count["fine"] = 1
                    word_active = "fine"
                if word_count["fine"] >= count_th:
                    # print("word_count",word_count["fine"])
                    word_count["fine"] = 0
                    # playsound(mp3_file)
                    cv2.putText(img, "Fine",(100, 100), font, 0.75,(255,0,255),2,cv2.LINE_AA)
                    result_text = result_text + " สบายดี"

                    talk["fine"] = True
                    for i in range(time_show_text):
                        # out.write(img)
                        img_list.append(img)
            
            elif word == "ชอบ/รัก" and talk["love"] == False:
 
                if word_active is None or word_active == "love":
                    word_count["love"] += 1
                else:
                    word_count["love"] = 1
                    word_active = "love"
                if word_count["love"] >= count_th:
                    # print("word_count",word_count["love"])
                    word_count["love"] = 0
                    # playsound(mp3_file)
                    cv2.putText(img, "Love",(100, 100), font, 0.75,(255,0,255),2,cv2.LINE_AA)
                    result_text = result_text + " ชอบ/รัก"

                    talk["love"] = True
                    for i in range(time_show_text):
                        # out.write(img)
                        img_list.append(img)
            elif word == "ถามชื่อ" and talk["name"] == False:
 
                if word_active is None or word_active == "name":
                    word_count["name"] += 1
                else:
                    word_count["name"] = 1
                    word_active = "name"
                if word_count["name"] >= count_th:
                    # print("word_count",word_count["name"])
                    word_count["name"] = 0
                    # playsound(mp3_file)
                    cv2.putText(img, "Name",(100, 100), font, 0.75,(255,0,255),2,cv2.LINE_AA)
                    result_text = result_text + " ถามชื่อ"

                    talk["name"] = True
                    for i in range(time_show_text):
                        # out.write(img)
                        img_list.append(img)
            elif word == "ถามนามสกุล" and talk["lestname"] == False:
               
                if word_active is None or word_active == "lastname":
                    word_count["lastname"] += 1
                else:
                    word_count["lastname"] = 1
                    word_active = "lastname"
                if word_count["lastname"] >= count_th:
                    # print("word_count",word_count["lastname"])
                    word_count["lastname"] = 0
                    # playsound(mp3_file)
                    cv2.putText(img, "Lestname",(100, 100), font, 0.75,(255,0,255),2,cv2.LINE_AA)
                    result_text = result_text + " ถามนามสกุล"

                    talk["lastname"] = True
                    for i in range(time_show_text):
                        # out.write(img)
                        img_list.append(img)
            elif word == "เข้าใจ" and talk["understand"] == False:
          
                if word_active is None or word_active == "understand":
                    word_count["understand"] += 1
                else:
                    word_count["understand"] = 1
                    word_active = "understand"
                if word_count["understand"] >= count_th:
                    # print("word_count",word_count["understand"])
                    word_count["understand"] = 0
                    # playsound(mp3_file)
                    cv2.putText(img, "Understand",(100, 100), font, 0.75,(255,0,255),2,cv2.LINE_AA)
                    result_text = result_text + " เข้าใจ"

                    talk["understand"] = True
                    for i in range(time_show_text):
                        # out.write(img)
                        img_list.append(img)
                    # if word != "":
            else:
                word_count = {
                    "sad":0,
                    "thank":0,
                    "fine":0,
                    "sorry":0,
                    "hello":0,
                    "goodluck":0,
                    "love":0,
                    "name":0,
                    "lestname":0,
                    "understand":0,
                }
            img_list.append(img)
            

            # out.write(img)
        # out.release()
    print("use time",time.time() - start_time)
    start_time = time.time()
    clips = [ImageClip(m).set_duration(1) for m in img_list]

    concat_clip = concatenate_videoclips(clips, method="compose")
    concat_clip.write_videofile(settings.MEDIA_ROOT + "/videos/" + out_name + ".mp4", fps=24)

    mp3_file = settings.MEDIA_ROOT + "/sound/" + out_name + ".mp3"
    
    tts = gTTS(result_text, lang='th')
    tts.save(mp3_file)

    my_clip = VideoFileClip(settings.MEDIA_ROOT + "/videos/" + out_name + ".mp4")
 
    audio_background = AudioFileClip(settings.MEDIA_ROOT + "/sound/" + out_name + ".mp3")
    print("create audio time",time.time() - start_time)

    result = my_clip.set_audio(audio_background)
    result.write_videofile(settings.MEDIA_ROOT + "/videos/result_" + out_name + ".webm",fps=30)

    print("create vdo time",time.time() - start_time)
    return "result_" + out_name, str(int(width*1.25)), str(int(height*1.25))

def reset_talk(self):
    talk = {
        "sad":False,
        "thank":False,
        "fine":False,
        "sorry":False,
        "hello":False,
        "goodluck":False,
        "love":False,
        "name":False,
        "lestname":False,
        "understand":False,
    }
