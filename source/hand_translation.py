import cv2
import numpy as np 
import time
from gtts import gTTS
from playsound import playsound
import os


h_min = 0
s_min = 50
v_min = 40
h_max = 30
s_max = 255
v_max = 255




face_cascade = cv2.CascadeClassifier("/home/worakit/hand_translation/source/haarcascade_frontalface_default.xml")

#  function
def convert_to_onehot(img_color):
    gray = cv2.cvtColor(img_color, cv2.COLOR_BGR2GRAY)  
    gray = cv2.resize(gray,(120,120))
    onehot = gray.ravel()/255.
    return onehot

def padding_frame(roi, w, h):
    if w < 120 and h < 120:
        size_black = 120
    elif w < 180 and h < 180:
        size_black = 180
    elif w < 240 and h < 240:
        size_black = 240   
    elif w < 360 and h < 360:
        size_black = 360  
    elif w < 480 and h < 480:
        size_black = 480
     
    half_size = size_black // 2

    hand_roi = np.zeros((size_black, size_black, 3), np.uint8)
    
    start_x = half_size - w//2
    end_x = half_size + w//2

    start_y = half_size - h//2
    end_y = half_size + h//2

    width = end_x - start_x
    height = end_y - start_y

    roi = cv2.resize(roi, (width, height))
    hand_roi[start_y:end_y, start_x:end_x] = roi
    
    return hand_roi

def translation(img, model):
    global graph, h_min, h_max, s_min, s_max, v_min, v_max, talk_thankyou, talk_sad, face_cascade
    
#การลด นอยด์ ของภาพ ยิ่งเพิ่มค่ายิ่งเบลอ
    blur = cv2.GaussianBlur(img, (5, 5), 0) 
# แปลง BGR เป็น GRAY 
    gray = cv2.cvtColor(blur, cv2.COLOR_BGR2GRAY)  
    face = face_cascade.detectMultiScale(gray, 1.3, 3)  
    
# สร้าง numpy array ที่มีค่า 0
    mask_face = np.zeros(gray.shape, np.uint8)  
    mask_face.fill(255)
    hsv=cv2.cvtColor(img,cv2.COLOR_BGR2HSV)  
    upper = np.array([h_max,s_max,v_max],np.uint8)
    lower = np.array([h_min,s_min,v_min],np.uint8)       
    mask_face_hand = cv2.inRange(hsv,lower,upper)
    rows, cols = mask_face_hand.shape
    cv2.rectangle(mask_face_hand, (cols - 100, 0), (cols, 100), (0), -1)
    
# MARKING THE DETECTED ROI
    for (x, y, w, h) in face:  
        cv2.rectangle(mask_face, (x, 0), (x + w + 5, y + h + 20), (0), -1)
    
    mask = cv2.bitwise_and(mask_face, mask_face_hand)
    color_mask = cv2.bitwise_and(img, img, mask=mask)
    contours, _ = cv2.findContours(mask, cv2.RETR_TREE,cv2.CHAIN_APPROX_NONE)
    count = 0
    font = cv2.FONT_HERSHEY_SIMPLEX
    hand_list = []
 
    hand = [0]*17

    for cnt in contours:
        area = cv2.contourArea(cnt)
        x,y,w,h = cv2.boundingRect(cnt) 
        if area < 2000:
            continue
        if x > cols//2 and y < 100:
            continue

        start_x = x
        end_x = x + w
        start_y = y
        end_y = y + h
        roi = color_mask[start_y:end_y, start_x:end_x]
        if w < 480 and h < 480:
            hand_roi = padding_frame(roi, w, h)
            onehot = convert_to_onehot(hand_roi)
            # print("test1")
            onehot = np.reshape(onehot.copy(), [1, 120*120])
   
            # print(onehot.shape)
            y = model.predict(onehot)

            # print("test3")

            y = np.argmax(y, axis=1)
            y = y[0]
            # y = 2
            cv2.putText(hand_roi, str(y),(20, 20), font, 0.75,(255,0,255),2,cv2.LINE_AA)
   
            cv2.waitKey(20)
            hand[y] = 1
        count+=1
    #เปลี่ยนการแสดงผลลัพธ์
    word = ""
    # print(hand)
    #เจอรูปมือแบบ folder 00 กับ 01 คือท่า ขอบคุณ
    if hand[0] == 1 or hand[1] == 1: 
        # cv2.putText(img, "Thank You",(100, 100), font, 0.75,(255,0,255),2,cv2.LINE_AA)
        word = "ขอบคุณ"
   

    #เจอรูปมือแบบ folder 02 กับ 03 คือท่า สบายดี
    elif hand[2] == 1 or hand[3] == 1:
        # cv2.putText(img, "Fine",(100, 100), font, 0.75,(255,0,255),2,cv2.LINE_AA)
        word = "สบายดี"

    #เจอรูปมือแบบ folder 04 กับ 05 คือท่า เสียใจ
    elif hand[4] == 1: #or hand[5] == 1:
        # cv2.putText(img, "Sad",(100, 100), font, 0.75,(255,0,255),2,cv2.LINE_AA)
        word = "เสียใจ"

    

    #เจอรูปมือแบบ folder 6 คือท่า โชคดี
    elif hand[6] == 1:
        # cv2.putText(img, "Good Luck",(100, 100), font, 0.75,(255,0,255),2,cv2.LINE_AA)
        word = "โชคดี"
    
    #เจอรูปมือแบบ folder 12 คือท่า สวัสดี
    elif hand[12] == 1:
        # cv2.putText(img, "Hello",(100, 100), font, 0.75,(255,0,255),2,cv2.LINE_AA)
        word = "สวัสดี"


    #เจอรูปมือแบบ folder 14 คือท่า ชื่อ    
    elif hand[14] == 1:
        # cv2.putText(img, "Name",(100, 100), font, 0.75,(255,0,255),2,cv2.LINE_AA)
        word = "ถามชื่อ"

    #เจอรูปมือแบบ folder 15 กับ 16 คือท่า นามสกุล
    elif hand[15] == 1 and hand[16] == 1:
    #     cv2.putText(img, "Lestname",(100, 100), font, 0.75,(255,0,255),2,cv2.LINE_AA)
        word = "ถามนามสกุล"

    #เจอรูปมือแบบ folder 8 คือท่า เข้าใจ    
    elif hand[8] == 1:
    #     cv2.putText(img, "Understand",(100, 100), font, 0.75,(255,0,255),2,cv2.LINE_AA)
        word = "เข้าใจ"

    #เจอรูปมือแบบ folder 13 คือท่า รัก    
    elif hand[13] == 1:
        # cv2.putText(img, "Love",(100, 100), font, 0.75,(255,0,255),2,cv2.LINE_AA)
        word = "ชอบ/รัก"
    #เจอรูปมือแบบ folder 11 คือท่า ขอโทษ
    elif hand[11] == 1:
        # cv2.putText(img, "Sorry",(100, 100), font, 0.75,(255,0,255),2,cv2.LINE_AA)
        word = "ขอโทษ"
    


    # cap.release()
    # cv2.destroyAllWindows()
    return img, word

